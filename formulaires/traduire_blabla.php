<?php

function formulaires_traduire_blabla_charger_dist() {
	$contexte = array(
		'traduire' => '',
		'traduction' => '',
	);
	return $contexte;
}



function formulaires_traduire_blabla_verifier_dist() {
	$erreurs = array();
	if (!_request('traduire')) {
		$erreurs['message_erreur'] = "Vous avez oublié d'écrire ! Votre clavier est cassé ?";
		$erreurs['traduire'] = "C'est là dedans qu'on écrit son texte !";
	}
	return $erreurs;
}


// http://ajax.googleapis.com/ajax/services/language/translate?v=1.0&q=hello%20world&langpair=en%7Cit
define('URL_GOOGLE_TRANSLATE', "http://ajax.googleapis.com/ajax/services/language/translate");

function formulaires_traduire_blabla_traiter_dist() {

	// creer l'url selon l'api google
	$texte = _request('traduire');
	$url = parametre_url(URL_GOOGLE_TRANSLATE,'v',"1.0",'&');
	$url = parametre_url($url,'langpair','fr|en','&');
	$url = parametre_url($url,'q',$texte,'&');

	// chargement du texte traduit par google (retour : json)
	include_spip('inc/distant');
	$trad = recuperer_page($url);

	// attention : php 5.2
	$trad = json_decode($trad, true); // true = retour array et non classe

	// recuperation du resultat si OK
	if ($trad['responseStatus'] != 200) {
		return array(
			"editable" => true,
			"message_erreur" => "Pas de chance, faux retour de l'ami Google !"
		);
	}

	// envoi au charger
	set_request('traduction', $trad['responseData']['translatedText']);

	// message
	return array(
		"editable" => true,
		"message_ok" => "Et voilà la traduction !",
	);
}


?>
